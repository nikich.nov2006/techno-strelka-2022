using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LIneEnable : MonoBehaviour
{
    public InputActionReference toggleReference = null;
    public GameObject lineHolder;
    private void Awake()
    {
        toggleReference.action.started += Toggle;
    }
    private void OnDestroy()
    {
        toggleReference.action.started -= Toggle;
    }
    private void Toggle(InputAction.CallbackContext context)
    {
        bool isActive = !lineHolder.activeSelf;
        lineHolder.SetActive(isActive);
    }
}
