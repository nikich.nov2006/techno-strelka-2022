using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public AudioSource audioSource;
    private float timeInMinutes;
    private float oldMinutes = -1f;
    public GameObject[] notes;

    // Update is called once per frame
    void Update()
    {
        if(ClockUI.minutes != oldMinutes)
        {
            timeInMinutes = ClockUI.minutes + ClockUI.hours * 60;
            if (timeInMinutes == 420f)
            {
                notes[0].GetComponent<Image>().color = Color.green;
                notes[0].GetComponent<Button>().interactable = true;
            }
            else if (timeInMinutes == 525f)
            {
                ChangeButton(0, 1);
            }
            else if (timeInMinutes == 620f)
            {
                ChangeButton(1, 2);
            }
            else if (timeInMinutes == 735f)
            {
                ChangeButton(2, 3);
            }
            oldMinutes = ClockUI.minutes;
        }
    }
    //My Voids
    public void HightLightObj(GameObject attach)
    {
        attach.GetComponent<Outline>().enabled = true;
    }
    public void playAudioClip(AudioClip audio)
    {
        audioSource.clip = audio;
        audioSource.Play();
        
    }
    public void ChangeButton(int last, int now)
    {
        notes[last].GetComponent<Image>().color = Color.white;
        notes[last].GetComponent<Button>().interactable = false;
        notes[now].GetComponent<Image>().color = Color.green;
        notes[now].GetComponent<Button>().interactable = true;
    }
}
