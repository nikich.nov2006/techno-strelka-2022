using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KashaEnter : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Kasha")
        {
            
            other.GetComponentInChildren<Animator>().Play("In");
            other.transform.gameObject.tag = "Food";
            
        }
    }
}
