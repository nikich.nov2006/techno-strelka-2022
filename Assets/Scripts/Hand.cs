using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class Hand : MonoBehaviour
{
    Animator animator;
    private float gripValue;
    private float gripCurent;
    private float triggerValue;
    private float triggerCurent;
    // Start is called before the first frame update
    void Start()
    {
        
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimateHand();
    }
    internal void SetGrip(float v)
    {
        gripValue = v;
    }
    internal void SetTrigger(float v)
    {
        triggerValue = v;
    }
    void AnimateHand()
    {
        if(gripCurent != gripValue)
        {
            gripCurent = Mathf.MoveTowards(gripCurent, gripValue, Time.deltaTime * 10);
            animator.SetFloat("Grip", gripValue);
        }
        if (triggerCurent != triggerValue)
        {
            triggerCurent = Mathf.MoveTowards(triggerCurent, triggerValue, Time.deltaTime * 10);
            animator.SetFloat("Trigger", triggerValue);
        }
    }
}
