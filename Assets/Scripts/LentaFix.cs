using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LentaFix : MonoBehaviour
{
	public GameObject obj;
	public Material change;
	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Lenta")
		{
			Destroy(other.gameObject);
			obj.GetComponent<MeshRenderer>().materials[2] = change;
		}
	}
}
