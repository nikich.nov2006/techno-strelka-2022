using UnityEngine;
using UnityEngine.Rendering.Universal;
using Pixelplacement;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScreenFade : MonoBehaviour
{
    public ForwardRendererData rendererData = null;
    [Range(0, 1)] public float alpha = 1.0f;
    [Range(0, 5)] public float duration = 0.5f;
    public float delay = 1;
    private Material fadeMaaterial = null;

    private void Start()
    {
        // Find the, and set the feature's material
        SetupFadeFeature();
        StartCoroutine(Fade("start", 0));
    }

    private void SetupFadeFeature()
    {
        ScriptableRendererFeature feature = rendererData.rendererFeatures.Find(item => item is ScreenFadeFeature);
        // Look for the screen fade feature

        // Ensure it's the correct feature
        if (feature is ScreenFadeFeature screenFade)
        {
            fadeMaaterial = Instantiate(screenFade.settings.material);
            screenFade.settings.runTimeMaterial = fadeMaaterial;
        }

        // Duplicate material so we don't change the renderer's asset
    }

    public float FadeIn()
    {
        // Fade to black
        Tween.ShaderFloat(fadeMaaterial, "_Alpha", 1, duration, 0);
        return duration;
    }

    public float FadeOut()
    {
        // Fade to clear
        Tween.ShaderFloat(fadeMaaterial, "_Alpha", 0, duration, 0);
        return duration;
    }
    public IEnumerator Fade(string toDo, int scene)
    {
        float duration = FadeIn();
        if (toDo == "changeScene")
        {
            yield return new WaitForSeconds(duration);
            SceneManager.LoadScene(scene);
        }
        else if (toDo == "start")
        {
            yield return new WaitForSeconds(duration);
            yield return new WaitForSeconds(delay);
            FadeOut();
        }
    }
    public void StartFadeCoroutine(string toDo)
    {
        string[] variable = toDo.Split(' ');
        StartCoroutine(Fade(variable[0], int.Parse(variable[1])));
    }
}
