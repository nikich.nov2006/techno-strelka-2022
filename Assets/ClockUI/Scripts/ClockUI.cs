﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockUI : MonoBehaviour {

    [SerializeField] public float REAL_SECONDS_PER_INGAME_DAY = 3600f;
    public static float hours;
    public static float minutes;
    private Transform clockHourHandTransform;
    private Transform clockMinuteHandTransform;
    private Text timeText;
    private float day;
    private Vector3 hoursRotation;
    private Vector3 minutesRotation;


    private void Awake() {
        clockHourHandTransform = transform.Find("hourHand");
        clockMinuteHandTransform = transform.Find("minuteHand");
        timeText = transform.Find("timeText").GetComponent<Text>();
        hoursRotation = clockHourHandTransform.eulerAngles;
        minutesRotation = clockMinuteHandTransform.eulerAngles;
    }

    private void FixedUpdate() {
        day += Time.deltaTime / REAL_SECONDS_PER_INGAME_DAY;

        float dayNormalized = day % 1f;
        float rotationDegreesPerDay = 360f;
        clockHourHandTransform.eulerAngles = new Vector3(hoursRotation.x + transform.eulerAngles.x, hoursRotation.y + transform.eulerAngles.y - 90, -dayNormalized * rotationDegreesPerDay + transform.eulerAngles.z - 210);

        float hoursPerDay = 15f;
        clockMinuteHandTransform.eulerAngles = new Vector3(minutesRotation.x + transform.eulerAngles.x, minutesRotation.y + transform.eulerAngles.y - 90, -dayNormalized * rotationDegreesPerDay * hoursPerDay + transform.eulerAngles.z);

        hours = Mathf.Floor(dayNormalized * hoursPerDay) + 7;
        string hoursString = (Mathf.Floor(dayNormalized * hoursPerDay) + 7f).ToString("00");

        float minutesPerHour = 60f;
        minutes = Mathf.Floor(((dayNormalized * hoursPerDay) % 1f) * minutesPerHour);
        string minutesString = Mathf.Floor(((dayNormalized * hoursPerDay) % 1f) * minutesPerHour).ToString("00");

        timeText.text = hoursString + ":" + minutesString;

    }

}
